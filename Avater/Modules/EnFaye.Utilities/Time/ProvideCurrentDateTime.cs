﻿// Copyright 2014 BSW Technology Consulting, released under the BSD license - see LICENSING.txt at the top of this repository for details

using System;
using System.Linq;
using System.Linq.Expressions;
using EnFaye.Helpers.JetBrains.Annotations;

namespace EnFaye.Helpers.Time
{
    [UsedImplicitly]
    public class ProvideCurrentDateTime : IProvideCurrentDateTime
    {
        public DateTime Now
        {
            get { return DateTime.Now; }
        }

        public DateTimeOffset NowOffset
        {
            get { return DateTimeOffset.Now; }
        }
    }
}