﻿// using System.Collections.Generic;
// using Bayeux;
// using Bayeux.Client;

// namespace GitterSharp.Realtime
// {
//     internal class TokenBayeuxProtocolExtension : AdapterExtension
//     {
//         public string Token { get; set; }

//         public TokenBayeuxProtocolExtension(string token)
//         {
//             this.Token = token;
//         }

//         public override bool Equals(IExtension other)
//         {
//             if (other is TokenBayeuxProtocolExtension tx)
//                 return this.Token == tx.Token;

//             else return false;
//         }

//         bool AddToken(IMutableMessage message)
//         {
//             var ext = message.GetExtension(true);
//             ext["token"] = Token;
//             return true;
//         }

//         public override bool Receive(IClientSession session, IMutableMessage message)
//         {
//             return true;
//         }

//         public override bool ReceiveMeta(IClientSession session, IMutableMessage message)
//         {
//             return true;
//         }

//         public override bool Send(IClientSession session, IMutableMessage message)
//         {
//             return AddToken(message);
//         }

//         public override bool SendMeta(IClientSession session, IMutableMessage message)
//         {
//             return AddToken(message);
//         }
//     }
// }
