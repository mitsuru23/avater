﻿using Newtonsoft.Json;

namespace GitterSharp.Model.Realtime
{
    public class RealtimeChatMessage
    {
        /// <summary>
        /// "create" or "update" or "remove"
        /// </summary>
        [JsonProperty("operation")]
        public string Operation { get; set; }

        [JsonProperty("model")]
        public Message Message { get; set; }

        public RealtimeChatMessage()
        {

        }

        public RealtimeChatMessage(Message message, string operationType)
        {
            this.Operation = operationType;
            this.Message = message;
        }
    }
}
