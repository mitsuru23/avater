using System;

namespace GitterSharp.Helpers
{
    public class ProxyObserver<TIn, TOut> : IObservable<TOut>, IObserver<TIn>, IDisposable
    {
        Func<TIn, TOut> _converter;
        IObserver<TIn> _input;
        private IObserver<TOut> _output;

        public ProxyObserver(Func<TIn, TOut> converter)
        {
            this._converter = converter;
        }

        public void Dispose()
        {
            _output?.OnCompleted();
            _output = null;
        }

        public void OnCompleted()
        {
            this.Dispose();
        }

        public void OnError(Exception error)
        {
            _output?.OnError(error);
        }

        public void OnNext(TIn value)
        {
            _output?.OnNext(_converter(value));
        }

        public IDisposable Subscribe(IObserver<TOut> output)
        {
            this._output = output;
            return this;
        }
    }
}