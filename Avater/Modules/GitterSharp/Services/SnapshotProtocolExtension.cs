using EnFaye.Client;
using EnFaye.Messages;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;


namespace GitterSharp.Services
{
    public class SnapshotProtocolExtension<T> : IFayeProtocolExtension
    {
        private IObserver<T> _observer;

        public Action EmptySnapshot { get; set; }

        public void Connect(IObserver<T> observer)
        {
            _observer = observer;
        }

        public void ReceivedMessage(BaseFayeMessage message)
        {
            if (_observer is null) return;
            if (message.Ext is null) return;
            if (message.Channel != "/meta/subscribe") return;
            if (!message.Ext.ContainsKey("snapshot")) return;

            var raw = (JArray)message.Ext["snapshot"];

            var snapshotData = raw.ToObject<List<T>>();

            if (snapshotData.Count == 0)
                EmptySnapshot?.Invoke();
            else
                foreach (var snd in snapshotData)
                    _observer.OnNext(snd);
        }

        public void SendingMessage(BaseFayeMessage message)
        {

        }
    }
}