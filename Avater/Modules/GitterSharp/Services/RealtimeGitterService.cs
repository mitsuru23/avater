﻿using EnFaye.Client;
using GitterSharp.Configuration;
using GitterSharp.Model.Realtime;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Reactive.Linq;


namespace GitterSharp.Services
{

    public class RealtimeGitterService : IRealtimeGitterService
    {
        private bool _connected;
        private FayeClient fayeCli;
        private IFayeConnection fayeConn;

        public async void Connect(string Token)
        {
            var extensions = new IFayeProtocolExtension[] { new TokenProtocolExtension(Token) };
            fayeCli = new FayeClient(Constants.FayeBaseUrl, extensions);

            fayeConn = await fayeCli.Connect();
        }


        public void Disconnect()
        {
            fayeConn.Disconnect();
        }

        public IObservable<RealtimeUserPresence> SubscribeToUserPresence(string roomId)
                        => new GitterFayeSubscription<RealtimeUserPresence>(fayeConn, $"/api/v1/rooms/{roomId}");

        public IObservable<RealtimeChatMessage> SubscribeToChatMessages(string roomId)
                        => new RealtimeChatMessageSubscriptionHandler(fayeConn, roomId);

        public IObservable<RealtimeRoomUser> SubscribeToRoomUsers(string roomId)
                        => new GitterFayeSubscription<RealtimeRoomUser>(fayeConn, $"/api/v1/rooms/{roomId}/users");

        public IObservable<RealtimeRoomEvent> SubscribeToRoomEvents(string roomId)
                        => new GitterFayeSubscription<RealtimeRoomEvent>(fayeConn, $"/api/v1/rooms/{roomId}/events");

        public IObservable<RealtimeReadBy> SubscribeToChatMessagesReadBy(string roomId, string messageId)
                        => new GitterFayeSubscription<RealtimeReadBy>(fayeConn, $"/api/v1/rooms/{roomId}/chatMessages/{messageId}/readBy");
    }
}