﻿using System;
using System.Collections.Generic;
using GitterSharp.Model;
using System.Reactive;
using GitterSharp.Configuration;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using GitterSharp.Helpers;
using GitterSharp.Model.Requests;
using GitterSharp.Model.Responses;
using System.Net.Http;
using System.Net.Http.Headers;

namespace GitterSharp.Services
{

    public class ReactiveGitterApiService : IReactiveGitterApiService
    {


        private readonly string _baseStreamingApiAddress = $"{Constants.StreamApiBaseUrl}{Constants.ApiVersion}";

        private HttpClient HttpClient
        {
            get
            {
                var httpClient = new HttpClient();

                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (!string.IsNullOrWhiteSpace(Token))
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                return httpClient;
            }
        }

        private IGitterApiService _apiService = new GitterApiService();





        public string Token
        {
            get { return _apiService.Token; }
            set { _apiService.Token = value; }
        }





        public ReactiveGitterApiService() { }

        public ReactiveGitterApiService(string token)
        {
            Token = token;
        }





        public IObservable<RepositoryInfo> GetRepositoryInfo(string repositoryName)
        {
            return _apiService.GetRepositoryInfoAsync(repositoryName).ToObservable();
        }





        public IObservable<GitterUser> GetCurrentUser()
        {
            return _apiService.GetCurrentUserAsync().ToObservable();
        }

        public IObservable<IEnumerable<Organization>> GetMyOrganizations(bool unused = false)
        {
            return _apiService.GetMyOrganizationsAsync(unused).ToObservable();
        }

        public IObservable<IEnumerable<Organization>> GetOrganizations(string userId)
        {
            return _apiService.GetOrganizationsAsync(userId).ToObservable();
        }

        public IObservable<IEnumerable<Repository>> GetMyRepositories(string query, int limit = 0)
        {
            return _apiService.GetMyRepositoriesAsync(query, limit).ToObservable();
        }

        public IObservable<IEnumerable<Repository>> GetMyRepositories(bool unused = false)
        {
            return _apiService.GetMyRepositoriesAsync(unused).ToObservable();
        }

        public IObservable<IEnumerable<Repository>> GetRepositories(string userId)
        {
            return _apiService.GetRepositoriesAsync(userId).ToObservable();
        }

        public IObservable<IEnumerable<Room>> GetSuggestedRooms()
        {
            return _apiService.GetSuggestedRoomsAsync().ToObservable();
        }

        public IObservable<IEnumerable<RoomUnreadCount>> GetAggregatedUnreadItems()
        {
            return _apiService.GetAggregatedUnreadItemsAsync().ToObservable();
        }

        public IObservable<UserInfo> GetUserInfo(string username)
        {
            return _apiService.GetUserInfoAsync(username).ToObservable();
        }





        public IObservable<UnreadItems> RetrieveUnreadChatMessages(string userId, string roomId)
        {
            return _apiService.RetrieveUnreadChatMessagesAsync(userId, roomId).ToObservable();
        }

        public IObservable<Unit> MarkUnreadChatMessages(string userId, string roomId, IEnumerable<string> messageIds)
        {
            return _apiService.MarkUnreadChatMessagesAsync(userId, roomId, messageIds).ToObservable();
        }





        public IObservable<IEnumerable<Room>> GetRooms()
        {
            return _apiService.GetRoomsAsync().ToObservable();
        }

        public IObservable<IEnumerable<GitterUser>> GetRoomUsers(string roomId, int limit = 30, string q = null, int skip = 0)
        {
            return _apiService.GetRoomUsersAsync(roomId, limit, q, skip).ToObservable();
        }

        public IObservable<Room> GetRoom(string roomName)
        {
            return _apiService.GetRoomAsync(roomName).ToObservable();
        }

        public IObservable<Room> JoinRoom(string userId, string roomId)
        {
            return _apiService.JoinRoomAsync(userId, roomId).ToObservable();
        }

        public IObservable<Room> UpdateRoom(string roomId, UpdateRoomRequest request)
        {
            return _apiService.UpdateRoomAsync(roomId, request).ToObservable();
        }

        public IObservable<bool> UpdateUserRoomSettings(string userId, string roomId, UpdateUserRoomSettingsRequest request)
        {
            return _apiService.UpdateUserRoomSettingsAsync(userId, roomId, request).ToObservable();
        }

        public IObservable<RoomNotificationSettingsResponse> GetRoomNotificationSettings(string userId, string roomId)
        {
            return _apiService.GetRoomNotificationSettingsAsync(userId, roomId).ToObservable();
        }

        public IObservable<RoomNotificationSettingsResponse> UpdateRoomNotificationSettings(string userId, string roomId, UpdateRoomNotificationSettingsRequest request)
        {
            return _apiService.UpdateRoomNotificationSettingsAsync(userId, roomId, request).ToObservable();
        }

        public IObservable<SuccessResponse> LeaveRoom(string roomId, string userId)
        {
            return _apiService.LeaveRoomAsync(roomId, userId).ToObservable();
        }

        public IObservable<SuccessResponse> DeleteRoom(string roomId)
        {
            return _apiService.DeleteRoomAsync(roomId).ToObservable();
        }

        public IObservable<IEnumerable<Room>> GetSuggestedRooms(string roomId)
        {
            return _apiService.GetSuggestedRoomsAsync(roomId).ToObservable();
        }

        public IObservable<IEnumerable<Collaborator>> GetSuggestedCollaboratorsOnRoom(string roomId)
        {
            return _apiService.GetSuggestedCollaboratorsOnRoomAsync(roomId).ToObservable();
        }

        public IObservable<IEnumerable<RoomIssue>> GetRoomIssues(string roomId)
        {
            return _apiService.GetRoomIssuesAsync(roomId).ToObservable();
        }

        public IObservable<IEnumerable<Ban>> GetRoomBans(string roomId)
        {
            return _apiService.GetRoomBansAsync(roomId).ToObservable();
        }

        public IObservable<BanUserResponse> BanUserFromRoom(string roomId, string username, bool removeMessages = false)
        {
            return _apiService.BanUserFromRoomAsync(roomId, username, removeMessages).ToObservable();
        }

        public IObservable<SuccessResponse> UnbanUser(string roomId, string userId)
        {
            return _apiService.UnbanUserAsync(roomId, userId).ToObservable();
        }

        public IObservable<WelcomeMessage> GetWelcomeMessage(string roomId)
        {
            return _apiService.GetWelcomeMessageAsync(roomId).ToObservable();
        }

        public IObservable<UpdateWelcomeMessageResponse> UpdateWelcomeMessage(string roomId, UpdateWelcomeMessageRequest request)
        {
            return _apiService.UpdateWelcomeMessageAsync(roomId, request).ToObservable();
        }

        public IObservable<InviteUserResponse> InviteUserInRoom(string roomId, InviteUserRequest request)
        {
            return _apiService.InviteUserInRoomAsync(roomId, request).ToObservable();
        }





        public IObservable<Message> GetSingleRoomMessage(string roomId, string messageId)
        {
            return _apiService.GetSingleRoomMessageAsync(roomId, messageId).ToObservable();
        }

        public IObservable<IEnumerable<Message>> GetRoomMessages(string roomId, MessageRequest request)
        {
            return _apiService.GetRoomMessagesAsync(roomId, request).ToObservable();
        }

        public IObservable<Message> SendMessage(string roomId, string message)
        {
            return _apiService.SendMessageAsync(roomId, message).ToObservable();
        }

        public IObservable<Message> UpdateMessage(string roomId, string messageId, string message)
        {
            return _apiService.UpdateMessageAsync(roomId, messageId, message).ToObservable();
        }

        public IObservable<Unit> DeleteMessage(string roomId, string messageId)
        {
            return _apiService.DeleteMessageAsync(roomId, messageId).ToObservable();
        }

        public IObservable<IEnumerable<GitterUser>> GetUsersWhoReadMessage(string roomId, string messageId)
        {
            return _apiService.GetUsersWhoReadMessageAsync(roomId, messageId).ToObservable();
        }





        public IObservable<IEnumerable<RoomEvent>> GetRoomEvents(string roomId, int limit = 50, int skip = 0, string beforeId = null)
        {
            return _apiService.GetRoomEventsAsync(roomId, limit, skip, beforeId).ToObservable();
        }





        public IObservable<IEnumerable<Group>> GetGroups()
        {
            return _apiService.GetGroupsAsync().ToObservable();
        }

        public IObservable<IEnumerable<Room>> GetGroupRooms(string groupId)
        {
            return _apiService.GetGroupRoomsAsync(groupId).ToObservable();
        }

        public IObservable<Room> CreateRoom(string groupId, CreateRoomRequest request)
        {
            return _apiService.CreateRoomAsync(groupId, request).ToObservable();
        }

        public IObservable<IEnumerable<Room>> GetSuggestedRoomsFromGroup(string groupId)
        {
            return _apiService.GetSuggestedRoomsFromGroupAsync(groupId).ToObservable();
        }





        public IObservable<SearchResponse<Room>> SearchRooms(string query, int limit = 10, int skip = 0)
        {
            return _apiService.SearchRoomsAsync(query, limit, skip).ToObservable();
        }

        public IObservable<SearchResponse<GitterUser>> SearchUsers(string query, int limit = 10, int skip = 0)
        {
            return _apiService.SearchUsersAsync(query, limit, skip).ToObservable();
        }

        public IObservable<SearchResponse<Repository>> SearchUserRepositories(string userId, string query, int limit = 10)
        {
            return _apiService.SearchUserRepositoriesAsync(userId, query, limit).ToObservable();
        }





        public IObservable<Message> GetRealtimeMessages(string roomId)
        {
            string url = _baseStreamingApiAddress + $"rooms/{roomId}/chatMessages";
            return HttpClient.CreateObservableHttpStream<Message>(url);
        }

        public IObservable<RoomEvent> GetRealtimeEvents(string roomId)
        {
            string url = _baseStreamingApiAddress + $"rooms/{roomId}/events";
            return HttpClient.CreateObservableHttpStream<RoomEvent>(url);
        }





        public IObservable<Dictionary<DateTime, int>> GetRoomMessagesCountByDay(string roomId)
        {
            return _apiService.GetRoomMessagesCountByDayAsync(roomId).ToObservable();
        }


    }
}