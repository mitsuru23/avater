using EnFaye.Client;
using GitterSharp.Helpers;
using GitterSharp.Model.Realtime;
using System;
using System.Reactive.Disposables;


namespace GitterSharp.Services
{
    public class RealtimeChatMessageSubscriptionHandler : IObservable<RealtimeChatMessage>, IDisposable
    {
        private IFayeConnection _fayeConn;
        private string _roomID;
        private CompositeDisposable _disposables = new CompositeDisposable();

        public RealtimeChatMessageSubscriptionHandler(IFayeConnection fayeConn, string roomID)
        {
            this._fayeConn = fayeConn;
            this._roomID = roomID;
        }
        public void Dispose()
        {
            _disposables?.Dispose();
        }
        public IDisposable Subscribe(IObserver<RealtimeChatMessage> observer)
        {
            var conv = new ProxyObserver<GitterSharp.Model.Message, RealtimeChatMessage>(x => new RealtimeChatMessage(x, "snapshot"));
            var spe = new SnapshotProtocolExtension<GitterSharp.Model.Message>();
            var sub = new GitterFayeSubscription<RealtimeChatMessage>(_fayeConn, $"/api/v1/rooms/{_roomID}/chatMessages", spe);

            spe.Connect(conv);
            spe.EmptySnapshot = () => observer?.OnNext(new RealtimeChatMessage(default, "empty"));
            _disposables.Add(conv.Subscribe(observer));
            _disposables.Add(sub.Subscribe(observer));

            return _disposables;
        }
    }
}