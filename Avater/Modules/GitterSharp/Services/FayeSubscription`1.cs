using EnFaye.Client;
// using GitterSharp.Realtime;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using static GitterSharp.Services.RealtimeGitterService;

namespace GitterSharp.Services
{
    internal class GitterFayeSubscription<T> : IObservable<T>, IDisposable
    {
        private IFayeConnection fayeConn;
        private string channelString;
        private IObserver<T> _observer;
        private IEnumerable<IFayeProtocolExtension> extensions;

        public GitterFayeSubscription(IFayeConnection fayeConn, string channelString, params IFayeProtocolExtension[] extensions)
        {
            this.fayeConn = fayeConn;
            this.channelString = channelString;
            this.extensions = extensions;
        }

        public void Dispose()
        {
            try
            {
                fayeConn?.Unsubscribe(channelString);
            }
            catch (Exception e)
            {
                _observer?.OnError(e);
            }
        }

        async void SubcribeCore()
        {
            try
            {
                await fayeConn.Subscribe(channelString, x =>
                {
                    try
                    {
                        _observer.OnNext(JsonConvert.DeserializeObject<T>(x));
                    }
                    catch (Exception e)
                    {
                        _observer.OnError(e);
                    }
                }, extensions);
            }
            catch (Exception e)
            {
                _observer.OnError(e);
            }
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            _observer = observer;
            SubcribeCore();
            return this;
        }
    }
}