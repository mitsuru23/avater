using GitterSharp.Model.Realtime;
// using GitterSharp.Realtime;
using System;

namespace GitterSharp.Services
{
    public interface IRealtimeGitterService
    {
        /// <summary>
        /// Connect to faye pub-sub system
        /// </summary>
        void Connect(string Token);

        /// <summary>
        /// Disconnect from faye pub-sub system
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Subscribe to get user presence (on or off) of a romm in realtime
        /// </summary>
        /// <param name="roomId">Id of the room</param>
        /// <returns></returns>
        IObservable<RealtimeUserPresence> SubscribeToUserPresence(string roomId);

        /// <summary>
        /// Subscribe to get chat messages of a romm in realtime (added, updated or removed)
        /// </summary>
        /// <param name="roomId">Id of the room</param>
        /// <returns></returns>
        IObservable<RealtimeChatMessage> SubscribeToChatMessages(string roomId);

        /// <summary>
        /// Subscribe to get users of a romm in realtime (added or removed)
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        IObservable<RealtimeRoomUser> SubscribeToRoomUsers(string roomId);

        /// <summary>
        /// Subscribe to get events of a romm in realtime
        /// </summary>
        /// <param name="roomId">Id of the room</param>
        /// <returns></returns>
        IObservable<RealtimeRoomEvent> SubscribeToRoomEvents(string roomId);

        /// <summary>
        /// Subscribe to get info of why read a message in a romm in realtime
        /// </summary>
        /// <param name="roomId">Id of the room</param>
        /// <param name="messageId">Id of the message</param>
        /// <returns></returns>
        IObservable<RealtimeReadBy> SubscribeToChatMessagesReadBy(string roomId, string messageId);
    }
}
