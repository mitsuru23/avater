using EnFaye.Client;
using EnFaye.Messages;
using System.Collections.Generic;


namespace GitterSharp.Services
{
    public class TokenProtocolExtension : IFayeProtocolExtension
    {
        private string token;

        public TokenProtocolExtension(string token)
        {
            this.token = token;
        }

        public void ReceivedMessage(BaseFayeMessage message)
        {

        }

        public void SendingMessage(BaseFayeMessage message)
        {
            if (message.Ext == null) message.Ext = new Dictionary<string, object>();
            message.Ext["token"] = token;
        }
    }
}