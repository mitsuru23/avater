// Copyright 2013 BSW Technology Consulting, released under the BSD license - see LICENSING.txt at the top of this repository for details


using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EnFaye.Helpers.Enum;

namespace EnFaye.Messages
{
    public class BaseFayeMessage
    {
        protected BaseFayeMessage(MetaChannels channel,
                                  string id)
            : this(channel: channel.StringValue(),
                   id: id)
        {
        }

        protected BaseFayeMessage(string channel,
                                  string id)
        {
            Channel = channel;
            Id = id;
        }

        // for JSON serializer
        public BaseFayeMessage()
        {
        }

        public string Channel { get; set; }
        public string Id { get; set; } 
        public Dictionary<string, object> Ext { get; set; }
    }
}