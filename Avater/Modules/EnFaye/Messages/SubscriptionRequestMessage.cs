// Copyright 2013 BSW Technology Consulting, released under the BSD license - see LICENSING.txt at the top of this repository for details


using System;
using System.Linq;
using System.Linq.Expressions;



namespace EnFaye.Messages
{
    internal class SubscriptionRequestMessage : BaseFayeMessage
    {
        public string ClientId { get; set; }
        public string Subscription { get; set; }

        public SubscriptionRequestMessage(string clientId,
                                          string subscriptionChannel,
                                          string id)
            : base(channel: MetaChannels.Subscribe,
                   id: id)
        {
            ClientId = clientId;
            Subscription = subscriptionChannel;
        }
    }
}