﻿// Copyright 2013 BSW Technology Consulting, released under the BSD license - see LICENSING.txt at the top of this repository for details


using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using EnFaye.Socket;
using EnWebSockets;



namespace EnFaye.Transports
{
    public class WebsocketTransportConnection : BaseWebSocket,
                                                ITransportConnection
    {
        private readonly Queue<string> _outgoingMessageQueue;
        private readonly object _connectionStateMutex;

        // 5 seconds
        private static readonly TimeSpan DefaultRetryTimeout = TimeSpan.FromSeconds(5);

        private readonly Func<TimeSpan> _connectionOpenTimeoutFetch;
        private readonly Action<TimeSpan> _connectionOpenTimeoutSetter;

        internal WebsocketTransportConnection(IWebSocket webSocket,
                                              Func<TimeSpan> connectionOpenTimeoutFetch,
                                              Action<TimeSpan> connectionOpenTimeoutSetter,
                                              string connectionId) : base(socket: webSocket,
                                                                          connectionId: connectionId)
        {
            _connectionOpenTimeoutSetter = connectionOpenTimeoutSetter;
            _connectionOpenTimeoutFetch = connectionOpenTimeoutFetch;
            Socket.MessageReceived += WebSocketMessageReceived;
            Socket.Closed += WebSocketClosedWithRetry;
            RetryTimeout = DefaultRetryTimeout;
            ConnectionState = ConnectionState.Connected;
            _outgoingMessageQueue = new Queue<string>();
            _connectionStateMutex = new object();
            RetryEnabled = true;
        }

        private void WebSocketClosedOnPurpose(object sender,
                                              EventArgs e)
        {
            Console.WriteLine("Connection close complete");
            lock (_connectionStateMutex)
            {
                ConnectionState = ConnectionState.Disconnected;
            }
            if (ConnectionClosed != null)
            {
                ConnectionClosed(this,
                                 new EventArgs());
            }
        }

        private void WebSocketClosedWithRetry(object sender,
                                              EventArgs e)
        {
            lock (_connectionStateMutex)
            {
                ConnectionState = ConnectionState.Lost;
            }
            if (RetryEnabled)
            {
                Console.WriteLine("Lost connection, retrying in {0} milliseconds",
                            RetryTimeout.TotalMilliseconds);
                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        ReestablishConnection().Wait();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Crashed on reconnect {e}");
                    }
                });
            }
            else
            {
                Console.WriteLine("Lost connection and not retrying");
            }
            if (ConnectionLost != null)
            {
                ConnectionLost(this,
                               new EventArgs());
            }
        }

        private async Task ReestablishConnection()
        {
            Thread.Sleep(RetryTimeout);
            Console.WriteLine("Retrying connection");
            lock (_connectionStateMutex)
            {
                ConnectionState = ConnectionState.Reconnecting;
            }
            await ConnectWebsocket();
            lock (_connectionStateMutex)
            {
                ConnectionState = ConnectionState.Connected;
            }
            if (ConnectionReestablished != null)
            {
                ConnectionReestablished(this,
                                        new EventArgs());
            }
            SendQueuedMessages();
        }

        private void SendQueuedMessages()
        {
            Console.WriteLine("Sending queued messages from when connection was lost");
            while (true)
            {
                string message;
                try
                {
                    // wait to dequeue until we successfully send the message
                    message = _outgoingMessageQueue.Peek();
                }
                catch (InvalidOperationException)
                {
                    break;
                }
                Send(message);
                _outgoingMessageQueue.Dequeue();
            }
        }

        private void WebSocketMessageReceived(object sender,
                                              MessageReceivedEventArgs e)
        {
            Console.WriteLine("Received raw message '{0}'",
                         e.Message);
            if (MessageReceived != null)
            {
                MessageReceived(sender,
                                new MessageReceivedArgs(e.Message));
            }
        }

        public void Send(string message)
        {
            Console.WriteLine("Sending message '{0}'",
                         message);
            lock (_connectionStateMutex)
            {
                if (ConnectionState == ConnectionState.Lost || ConnectionState == ConnectionState.Reconnecting)
                {
                    Console.WriteLine("Connection was lost, queuing message");
                    _outgoingMessageQueue.Enqueue(message);
                }
                else
                {
                    Socket.Send(message);
                }
            }
        }

        public async Task Disconnect()
        {
            var considerDisconnected = (ConnectionState == ConnectionState.Disconnected) ||
                                       (ConnectionState == ConnectionState.Lost && !RetryEnabled);
            if (considerDisconnected)
            {
                Console.WriteLine("Already disconnected!");
                return;
            }
            Console.WriteLine("Disconnecting from websocket server");
            // We don't need the retry handler anymore
            DisableRetryHandler();
            var tcs = new TaskCompletionSource<bool>();
            EventHandler closed = (sender,
                                   args) => tcs.SetResult(true);
            Socket.Closed += closed;
            Socket.Close("Disconnection Requested");
            await tcs.Task;
        }

        public event MessageReceived MessageReceived;
        public event ConnectionEvent ConnectionClosed;
        public event ConnectionEvent ConnectionLost;
        public event ConnectionEvent ConnectionReestablished;
        public TimeSpan RetryTimeout { get; set; }
        public bool RetryEnabled { get; set; }
        public ConnectionState ConnectionState { get; private set; }

        private void DisableRetryHandler()
        {
            Socket.Closed -= WebSocketClosedWithRetry;
            Socket.Closed += WebSocketClosedOnPurpose;
        }

        public void NotifyOfPendingServerDisconnection()
        {
            DisableRetryHandler();
        }

        public override TimeSpan ConnectionOpenTimeout
        {
            get { return _connectionOpenTimeoutFetch(); }
            set { _connectionOpenTimeoutSetter(value); }
        }
    }
}