// Copyright 2013 BSW Technology Consulting, released under the BSD license - see LICENSING.txt at the top of this repository for details


using System;
using System.Linq;
using EnFaye.Messages;
using EnFaye.Serialization;
using EnFaye.Transports;
using EnFaye.Helpers.Enum;



namespace EnFaye.Client
{
    public abstract class FayeClientBase
    {
        internal const string ONLY_SUPPORTED_CONNECTION_TYPE = "websocket";

        internal const string CONNECTION_TYPE_ERROR_FORMAT =
            "We only support 'websocket' and the server only supports [{0}] so we cannot communicate";

        protected readonly FayeJsonConverter Converter;
        protected int MessageCounter;

        private static readonly TimeSpan DefaultHandshakeTimeout = TimeSpan.FromSeconds(10);

        protected FayeClientBase(int messageCounter)
            : this(messageCounter,
                   DefaultHandshakeTimeout)
        {
        }

        protected FayeClientBase(int messageCounter,
                                 TimeSpan handshakeTimeout)
        {
            Converter = new FayeJsonConverter();
            MessageCounter = messageCounter;
            HandshakeTimeout = handshakeTimeout;
        }

        public TimeSpan HandshakeTimeout { get; set; }

        protected void SendConnect(string clientId,
                                   ITransportConnection connection)
        {
            var message = new ConnectRequestMessage(clientId: clientId,
                                                    connectionType: ONLY_SUPPORTED_CONNECTION_TYPE,
                                                    id: (MessageCounter++).ToString());
            var json = Converter.Serialize(message);
            connection.Send(json);
        }

        private static TimeSpan FromMilliSecondsStr(string milliseconds)
                    => TimeSpan.FromMilliseconds(Convert.ToInt32(milliseconds));


        protected static Advice ParseAdvice(dynamic message)
        {
            if (message.advice == null) return null;
            var advice = message.advice;
            var timeout = FromMilliSecondsStr((string)advice.timeout);
            var interval = FromMilliSecondsStr((string)advice.interval);
            var reconnect = ((string)advice.reconnect).EnumValue<Reconnect>();
            return new Advice(reconnect: reconnect,
                              interval: interval,
                              timeout: timeout);
        }

        protected static void SetRetry(Advice advice,
                                       ITransportConnection transportConnection)
        {
            transportConnection.RetryEnabled = advice.Reconnect != Reconnect.None;
        }
    }
}