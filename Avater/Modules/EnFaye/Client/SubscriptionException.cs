// Copyright 2013 BSW Technology Consulting, released under the BSD license - see LICENSING.txt at the top of this repository for details


using System;
using System.Linq;
using System.Linq.Expressions;



namespace EnFaye.Client
{
    public class SubscriptionException : Exception
    {
        public SubscriptionException(string serverError) : base(serverError)
        {
        }
    }
}