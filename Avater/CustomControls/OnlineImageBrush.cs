﻿using Avalonia;
using Avalonia.Controls;
using System;
using Avalonia.Media.Imaging;
using System.Net;
using System.IO;
using Avalonia.Media;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Threading;
using Avater.Helpers;
using Avalonia.Threading;

namespace Avater.Helpers
{
    public class BackgroundQueue
    {
        private Task previousTask = Task.FromResult(true);
        private object key = new object();

        public Task QueueTask<T>(Action<T> action, T val)
        {
            lock (key)
            {
                previousTask = previousTask.ContinueWith(
                  t => action(val),
                  CancellationToken.None,
                  TaskContinuationOptions.None,
                  TaskScheduler.Default);
                return previousTask;
            }
        }

        public Task<T> QueueTask<T>(Func<T> work)
        {
            lock (key)
            {
                var task = previousTask.ContinueWith(
                  t => work(),
                  CancellationToken.None,
                  TaskContinuationOptions.None,
                  TaskScheduler.Default);
                previousTask = task;
                return task;
            }
        }
    }
}


namespace Avater.CustomControls
{
    public class OnlineImageBrush : ImageBrush
    {
        public static readonly StyledProperty<string> ImageUrlProperty =
            AvaloniaProperty.Register<OnlineImageBrush, string>(nameof(ImageUrl));
        public string ImageUrl
        {
            get { return GetValue(ImageUrlProperty); }
            set { SetValue(ImageUrlProperty, value); }
        }

        static OnlineImageBrush()
        {
            ImageUrlProperty.Changed.AddClassHandler<OnlineImageBrush>(Hndlr);
        }

        private static readonly BackgroundQueue bq = new BackgroundQueue();

        private static readonly WebClient webClient = new WebClient();

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                var inputBytes = Encoding.ASCII.GetBytes(input);
                var hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                var sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        private static void Hndlr(OnlineImageBrush obj, AvaloniaPropertyChangedEventArgs arg2)
        {
            var url = (String)arg2.NewValue;
            if (String.IsNullOrEmpty(url))
                return;

            var uri = new Uri(url);

            var localFile = Path.Combine(Path.GetTempPath(), CreateMD5(url));

            if (File.Exists(localFile))
            {
                SetSource((OnlineImageBrush)obj, localFile);
            }
            else
            {
                bq.QueueTask(async (x) =>
                {
                    try
                    {
                        webClient.DownloadFile(x.Item1, x.Item2);
                        await Task.Delay(100);
                        Dispatcher.UIThread.Post(() =>
                        {
                            SetSource((OnlineImageBrush)obj, localFile);
                        });
                    }
                    catch
                    {
                        File.Delete(localFile);
                    }
                    finally
                    {

                    }
                }, (uri, localFile));
            }
        }

        private static void SetSource(OnlineImageBrush inst, String path)
        {
            try
            {
                inst.Source = new Bitmap(path);
            }
            catch
            {

            }
        }
    }
}