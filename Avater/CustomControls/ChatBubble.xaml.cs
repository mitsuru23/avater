using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Net.Http;
using System;
using System.Diagnostics;
using Avater.Models.EnvelopTypes;

using ReactiveUI;
using System.Reactive.Linq;

namespace Avater.CustomControls
{
    public class ChatBubble : UserControl
    {

        public static readonly DirectProperty<ChatBubble, UIMessage.ChatPositionType> PositionTypeProperty =
        AvaloniaProperty.RegisterDirect<ChatBubble, UIMessage.ChatPositionType>(
            nameof(PositionType),
            o => o.PositionType,
            (o, v) => o.PositionType = v);

        private UIMessage.ChatPositionType _posType;

        public UIMessage.ChatPositionType PositionType
        {
            get { return _posType; }
            set { SetAndRaise(PositionTypeProperty, ref _posType, value); }
        }

        public ChatBubble()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}