using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Net.Http;
using System.Diagnostics;
using ReactiveUI;
using System.Reactive.Linq;
using GitterSharp.Model;
using Avalonia.Threading;
using System.Threading.Tasks;

namespace Avater.CustomControls
{

    public class RoomAvatar : UserControl
    {
        public RoomAvatar()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}