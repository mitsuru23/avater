using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using System.Net.Http;
using System;
using System.Diagnostics;
using Avalonia.Interactivity;
using ReactiveUI;
using Avater.ViewModels;
using System.Reactive.Linq;


namespace Avater.Pages
{
    public class MainPage : UserControl
    {
        // public static readonly DirectProperty<MainPage, Vector> ChatBoxOffsetProperty =
        //     AvaloniaProperty.RegisterDirect<MainPage, Vector>(
        //         nameof(ChatBoxOffset),
        //         o => o.ChatBoxOffset,
        //         (o, v) => o.ChatBoxOffset = v);

        // Vector _chatBoxOffset;

        // public Vector ChatBoxOffset
        // {
        //     get => _chatBoxOffset;
        //     set => SetAndRaise(ChatBoxOffsetProperty, ref _chatBoxOffset, value);
        // }

        public MainPage()
        {
            InitializeComponent();

            this.FindControl<ListBox>("PART_RoomBox")
                .WhenAny(p => p.Scroll, p => p);
                
            var chatBox =
            this.FindControl<ScrollViewer>("PART_ChatBox");

            chatBox.WhenAnyValue(p => p.Extent, p => p.Viewport, p => p.Offset)
                   .Subscribe(x =>
                   {
                       if (this.DataContext is MainPageViewModel vm)
                       {
                           vm.ChatBoxMaxOffsets = new Size(x.Item1.Width - x.Item2.Width, x.Item1.Height - x.Item2.Height);
                       }
                   });

            this.DataContextChanged += MainPage_OnDataContextChanged;
        }

        IDisposable dtSidebar = null;

        private void MainPage_OnDataContextChanged(object sender, EventArgs e)
        {
            if (this.DataContext.GetType() == typeof(MainPageViewModel))
            {
                var dt = (MainPageViewModel)DataContext;

                dtSidebar?.Dispose();

                dtSidebar = dt.WhenAnyValue(x => x.IsSidebarClosed)
                              .DistinctUntilChanged()
                              .Subscribe(x => OnSidebarToggle(x));
            }
        }

        public void OnSidebarToggle(bool isClosed)
        {
            var classes = this.FindControl<Grid>("PART_Sidebar").Classes;

            if (isClosed)
            {
                classes.Add("SDOpen");
                classes.Remove("SDClose");
            }
            else
            {
                classes.Add("SDClose");
                classes.Remove("SDOpen");
            }
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
            var k = new ListBox();
        }
    }
}