using System;

namespace Avater.Models
{
    public class StoreFileAttribute : Attribute
    {
        public StoreFileAttribute(string targetFile) => this.TargetFile = targetFile;

        public string TargetFile { get; private set; } 
    }
}