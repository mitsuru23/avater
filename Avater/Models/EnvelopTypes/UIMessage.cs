using Avalonia;
using GitterSharp.Model;
using ReactiveUI;

namespace Avater.Models.EnvelopTypes
{
    public class UIMessage : ReactiveObject
    {
        public UIMessage(Message msg)
        {
            this.Message = msg;
        }

        ChatPositionType _cpt;
        public ChatPositionType PositionType
        {
            get => _cpt;
            set => this.RaiseAndSetIfChanged(ref _cpt, value);
        }

        public enum ChatPositionType
        {
            Single,
            Middle,
            Start,
            Trail,
        }

        bool _isUser;
        public bool IsUser
        {
            get => _isUser;
            set => this.RaiseAndSetIfChanged(ref _isUser, value);
        }

        Message _msg;
        public Message Message
        {
            get => _msg;
            set => this.RaiseAndSetIfChanged(ref _msg, value);
        }
    }
}