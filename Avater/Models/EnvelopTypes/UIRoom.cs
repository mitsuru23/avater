using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Avalonia.Reactive;
using System.Reactive.Linq;
using ReactiveUI;
using Avater.Models.Interfaces;
using GitterSharp.Services;
using GitterSharp.Model;
using GitterSharp.Model.Requests;
using System.Linq;

using System.Collections.ObjectModel;
using Avalonia.Threading;
using Avalonia.Controls;

namespace Avater.Models.EnvelopTypes
{
    public class UIRoom
    {
        public Room RoomData { get; set; }

        public string Name => RoomData?.Name;

        public string Url => RoomData?.Url.Trim('/');


    }
}