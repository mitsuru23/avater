using System;
using Avater.ViewModels;

namespace Avater.Models.Interfaces
{
    public interface IPageRoot
    {
        void Navigate(ViewModelBase target, bool AddToHistory = false);
        void Back();
    }
}