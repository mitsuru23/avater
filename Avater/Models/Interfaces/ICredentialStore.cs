using System;
using GitterSharp.Model;

namespace Avater.Models.Interfaces
{
   public  interface ICredentialStore 
    {
        
        GitterUser UserInfo { get; set; }
        GitterToken AccessToken { get; set; }
    }
}