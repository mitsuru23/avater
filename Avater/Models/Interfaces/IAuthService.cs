
using System;
using System.Threading.Tasks;

namespace Avater.Models.Interfaces
{
    public interface IOAuthService : IObservable<GitterToken>, IDisposable
    {
        Task OAuthLogin();
    }
}