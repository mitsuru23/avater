using System;

namespace Avater.Models
{
    public class ExpiresAttribute : Attribute
    {

        public ExpiresAttribute(string expiry = default)
        {
            if (expiry != default)
                try
                {
                    var res = TimeSpan.Parse(expiry);
                    ExpiryTime = res;
                    return;
                }
                catch (Exception e)
                {
                    Serilog.Log.Error(e, "ExpiryAttribute Parse Exception");
                }

            this.ExpiryTime = TimeSpan.FromDays(3);

        }

        public TimeSpan ExpiryTime { get; private set; }
    }
}