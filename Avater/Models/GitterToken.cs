using System;
using Newtonsoft.Json;

namespace Avater.Models
{
    public class GitterToken
    {
        [JsonIgnore]
        public bool IsSuccessful { get; set; } = false;

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
}