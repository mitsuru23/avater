using System;
using Avalonia.Data.Converters;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;

namespace Avater.Converters
{
    public class BooleanAndConverter : IMultiValueConverter
    { 
        object IMultiValueConverter.Convert(IList<object> values, Type targetType, object parameter, CultureInfo culture)
        {
            return values.OfType<IConvertible>().All(System.Convert.ToBoolean);
        }
    }
}
