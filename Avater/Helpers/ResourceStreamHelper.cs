using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Platform;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading.Tasks;

namespace Avater
{
    public static class ResourceStreamHelper
    {
        public static async Task<string> GetStringFromAvares(string avares)
        {

            var assetLocator = AvaloniaLocator.Current.GetService<IAssetLoader>();
            using var stream = assetLocator.Open(new Uri(avares));
            using var streamx = new StreamReader(stream);
            return await streamx.ReadToEndAsync();
            
        }
    }
}