using System;
using System.Linq;
using System.Reflection;
using Avalonia;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using Avater.Models;
using Avater.Models.Interfaces;
using Avater.ViewModels;
using GitterSharp.Services;
using SimpleInjector;
using SimpleInjector.Advanced;
using Avalonia.Logging;
using Avalonia.Media.Imaging;

namespace Avater
{
    public class Services
    {
        /// Custom constructor resolution behavior
        public class ChooseLeastParamsBehaviour : IConstructorResolutionBehavior
        {
            public ConstructorInfo GetConstructor(Type implementationType) => (
                from ctor in implementationType.GetConstructors().Cast<ConstructorInfo>()
                orderby ctor.GetParameters().Length ascending
                select ctor)
                .First();
        }

        public static Container IoC { get; private set; }

        public static void Initialize()
        {
            IoC = new Container();

            IoC.Options.ConstructorResolutionBehavior = new ChooseLeastParamsBehaviour();

            // Initialize Singletons (Sorry, if you know of better way of doing this, ping me up!)
            IoC.Register<IPageRoot, MainWindowViewModel>(Lifestyle.Singleton);
            IoC.Register<IReactiveGitterApiService, ReactiveGitterApiService>(Lifestyle.Singleton);
            IoC.Register<IRealtimeGitterService, RealtimeGitterService>(Lifestyle.Singleton);
            IoC.Register<ICredentialStore, CredentialStore>(Lifestyle.Singleton);
            IoC.Register<IOAuthService, GitterAuth>(Lifestyle.Singleton);
            // IoC.Register<ICacheService<Uri>, ImageCacheService>(Lifestyle.Singleton);
            IoC.Verify();
        }               
    }
}