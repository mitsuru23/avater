﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Avalonia.Reactive;
using System.Reactive.Linq;
using ReactiveUI;
using Avater.Models.Interfaces;


namespace Avater.ViewModels
{
    public class MainWindowViewModel : ViewModelBase, IPageRoot
    {
        Stack<ViewModelBase> _history = new Stack<ViewModelBase>();

        ViewModelBase _CurrentView = new LoginPageViewModel();
        public ViewModelBase CurrentView
        {
            get => _CurrentView;
            set => this.RaiseAndSetIfChanged(ref _CurrentView, value);
        }

        void IPageRoot.Back()
        {
            if (_history.Count > 0)
            {
                CurrentView = _history.Pop();
            }
        }

        void IPageRoot.Navigate(ViewModelBase target, bool AddToHistory)
        {
            if (AddToHistory)
            {
                _history.Push(target);
            }
            CurrentView = target;
        }
    }
}