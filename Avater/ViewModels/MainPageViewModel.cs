using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Avalonia.Reactive;
using System.Reactive.Linq;
using ReactiveUI;
using Avater.Models.Interfaces;
using GitterSharp.Services;
using GitterSharp.Model;
using GitterSharp.Model.Requests;
using System.Linq;

using System.Collections.ObjectModel;
using Avalonia.Threading;
using Avalonia.Controls;
using Avater.Models.EnvelopTypes;
using Avalonia;
using System.Reactive.Disposables;
using Avalonia.Collections;

namespace Avater.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private readonly ICredentialStore _credStore;
        private readonly IReactiveGitterApiService _apiService;
        private IRealtimeGitterService _rtService;
        public GitterUser UserInfo => _credStore.UserInfo;
        public ObservableCollection<Room> RoomList { get; set; } = new ObservableCollection<Room>();
        public AvaloniaList<UIMessage> MessageList { get; set; } = new AvaloniaList<UIMessage>();


        Room _SelectedRoom;
        public Room SelectedRoom
        {
            get => _SelectedRoom;
            set => this.RaiseAndSetIfChanged(ref _SelectedRoom, value);
        }

        Vector _ChatBoxOffset;
        public Vector ChatBoxOffset
        {
            get => _ChatBoxOffset;
            set => this.RaiseAndSetIfChanged(ref _ChatBoxOffset, value);
        }

        bool _IsSidebarClosed;
        public bool IsSidebarClosed
        {
            get => _IsSidebarClosed;
            set => this.RaiseAndSetIfChanged(ref _IsSidebarClosed, value);
        }

        bool _IsRoomLoading;
        public bool IsRoomLoading
        {
            get => _IsRoomLoading;
            set => this.RaiseAndSetIfChanged(ref _IsRoomLoading, value);
        }

        bool _IsLoadingPreviousMessages;

        public bool IsLoadingPreviousMessages
        {
            get => _IsLoadingPreviousMessages;
            set => this.RaiseAndSetIfChanged(ref _IsLoadingPreviousMessages, value);
        }

        readonly ObservableAsPropertyHelper<bool> _isRoomEmpty;
        public bool IsRoomEmpty
        {
            get { return _isRoomEmpty?.Value ?? false; }
        }

        Size _chatBoxMaxOffsets;
        public Size ChatBoxMaxOffsets
        {
            get => _chatBoxMaxOffsets;
            set
            {
                if (_newMessage)
                {
                    _newMessage = false;

                    if (_ignoreBottomScrollLock & value.Height > 0)
                    {
                        ChatBoxOffset = new Vector(ChatBoxOffset.X, value.Height);
                        _ignoreBottomScrollLock = false;
                    }
                    else
                    {
                        if (WithinPercent(ChatBoxOffset.Y, _chatBoxMaxOffsets.Height, 10))
                            ChatBoxOffset = new Vector(ChatBoxOffset.X, value.Height);
                    }
                }

                _chatBoxMaxOffsets = value;
            }
        }

        public void ToggleSidebar() => IsSidebarClosed = !IsSidebarClosed;

        static bool _isFirstRun = true;
        bool _newMessage, _ignoreBottomScrollLock;
        CompositeDisposable _diposables;

        bool WithinPercent(double n1, double n2, double percentage) =>
            n2 == 0.0 ? false : (percentage > Math.Abs(Math.Abs(n2 - n1) / n2) * 100.0);

        public MainPageViewModel()
        {
            this._credStore = Services.IoC.GetInstance<ICredentialStore>();
            this._apiService = Services.IoC.GetInstance<IReactiveGitterApiService>();
            this._rtService = Services.IoC.GetInstance<IRealtimeGitterService>();

            _apiService.GetRooms()
                       .ObserveOn(AvaloniaScheduler.Instance)
                       .Subscribe(p => RoomsListener(p));

            this.WhenAnyValue(p => p.SelectedRoom)
                .DistinctUntilChanged()
                .Do(x => MessageList.Clear())
                .Do(x => IsRoomLoading = true)
                .Where(p => p != null && _apiService != null)
                .Subscribe(p => InitializeChatBoxRoom(p));

            _isRoomEmpty = Observable.Merge(this.WhenAnyValue(p => p.IsRoomLoading),
                                            this.WhenAnyValue(p => p.IsLoadingPreviousMessages),
                                            MessageList.WhenAnyValue(x => x.Count)
                                                       .Select(x => x == 0))
                                     .Select(x => !IsRoomLoading &
                                                  !IsLoadingPreviousMessages &
                                                   MessageList.Count == 0)
                                     .DistinctUntilChanged()
                                     .ToProperty(this, x => x.IsRoomEmpty);

            this.WhenAnyValue(p => p.ChatBoxOffset)
                .DistinctUntilChanged()
                .Select(x => x.Y == 0 &
                            !IsLoadingPreviousMessages &
                            !IsRoomLoading &
                            !IsRoomEmpty &
                            SelectedRoom != null)
                .Where(p => p)
                .Do(LoadPreviousMessages)
                .Subscribe();
        }

        private void LoadPreviousMessages(bool obj)
        {
            IsLoadingPreviousMessages = true;

            var req = new MessageRequest()
            {
                BeforeId = MessageList.First().Message.Id
            };

            _apiService
                .GetRoomMessages(SelectedRoom.Id, req)
                .ObserveOn(AvaloniaScheduler.Instance)
                .Do(x => IsLoadingPreviousMessages = false)
                .Subscribe(p =>
                {
                    if (p is null) return;
                    
                    foreach (var x in p.Reverse())
                    {
                        MessageList.Insert(0, new UIMessage(x));
                    }
                    CalcChatPositionTypes();
                 })
                .DisposeWith(_diposables);
        }

        private void InitializeChatBoxRoom(Room room)
        {
            _diposables?.Dispose();
            _diposables = new CompositeDisposable();

            _rtService
                .SubscribeToChatMessages(room.Id)
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(x =>
                {
                    var oldItem = MessageList.FirstOrDefault(p => p.Message.Id == x.Message.Id);

                    switch (x.Operation)
                    {
                        case "create":
                            MessageList.Add(new UIMessage(x.Message));
                            _newMessage = true;
                            break;
                        case "update":
                            if (oldItem is null) return;
                            var oldIndex = MessageList.IndexOf(oldItem);
                            MessageList[oldIndex] = new UIMessage(x.Message);
                            break;
                        case "remove":
                            if (oldItem is null) return;
                            MessageList.Remove(oldItem);
                            break;
                        case "snapshot":
                            _ignoreBottomScrollLock = true;
                            _newMessage = true;

                            if (oldItem is null)
                            {
                                MessageList.Add(new UIMessage(x.Message));
                            }
                            else
                            {
                                var oldIndex_ = MessageList.IndexOf(oldItem);
                                MessageList[oldIndex_] = new UIMessage(x.Message);
                            }

                            if (IsRoomLoading)
                                IsRoomLoading = false;

                            break;
                        case "empty":
                            IsRoomLoading = false;
                            break;

                    }
                    CalcChatPositionTypes();
                })
                .DisposeWith(_diposables);
        }

        private void CalcChatPositionTypes()
        {
            if (MessageList.Count < 2) return;

            for (int i = 0; i < MessageList.Count; i++)
            {
                var curMsg = MessageList[i];

                if (i == 0 || i + 1 == MessageList.Count)
                    continue;

                var prevMsg = MessageList[i - 1];
                var nxtMsg = MessageList[i + 1];
                var q1 = curMsg.Message.User.Id != prevMsg.Message.User.Id;
                var q2 = (curMsg.Message.User.Id != nxtMsg.Message.User.Id);

                if (q1 & !q2)
                {
                    curMsg.PositionType = UIMessage.ChatPositionType.Trail;
                }
                else if (!q1 & q2)
                {
                    curMsg.PositionType = UIMessage.ChatPositionType.Start;
                }
                else if (!q1 & !q2)
                {
                    curMsg.PositionType = UIMessage.ChatPositionType.Middle;
                }
                else if (q1 & q2)
                {
                    curMsg.PositionType = UIMessage.ChatPositionType.Single;
                }
            }
        }

        private void RoomsListener(IEnumerable<Room> p)
        {
            RoomList.Clear();

            foreach (var msg in p.OrderByDescending(x => x.LastAccessTime))
                RoomList.Add(msg);

            if (_isFirstRun && RoomList.Count > 0)
            {
                _isFirstRun = false;
                Dispatcher.UIThread.Post(async () => { await Task.Delay(250); SelectedRoom = RoomList.First(); });
            }
        }
    }
}