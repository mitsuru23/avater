﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Avalonia.Reactive;
using System.Reactive.Linq;
using ReactiveUI;
using Avater;
using Avater.Models.Interfaces;
using Avater.Models;
using GitterSharp.Services;


namespace Avater.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {
        ICredentialStore _credStore;
        IReactiveGitterApiService _apiService;
        private IRealtimeGitterService _rtService;

        bool _LoginButtonEnabled;
        public bool LoginButtonEnabled
        {
            get => _LoginButtonEnabled;
            set => this.RaiseAndSetIfChanged(ref _LoginButtonEnabled, value);
        }

        public LoginPageViewModel()
        {
            LoginButtonEnabled = false;

            this._credStore = Services.IoC.GetInstance<ICredentialStore>();
            this._apiService = Services.IoC.GetInstance<IReactiveGitterApiService>();
            this._rtService = Services.IoC.GetInstance<IRealtimeGitterService>();

            if (_credStore.AccessToken != null)
            {
                LoggedIn(_credStore.AccessToken);
            }
            else
            {
                LoginButtonEnabled = true;
            }

            Services.IoC.GetInstance<IOAuthService>()
                   .Where(p => p.IsSuccessful)
                   .Do(p => LoginButtonEnabled = false)
                   .Subscribe(p => LoggedIn(p));

        }

        private void LoggedIn(GitterToken p)
        {
            _apiService.Token = p.AccessToken;
            _rtService.Connect(p.AccessToken);
            
            _credStore.AccessToken = p;
            _apiService.GetCurrentUser()
                       .Subscribe(UserInfo =>
                       {
                           _credStore.UserInfo = UserInfo;
                           Services.IoC
                                   .GetInstance<IPageRoot>()
                                   .Navigate(new MainPageViewModel());
                       });
        }

        public async Task DoLogin()
        {
            await Services.IoC
                          .GetInstance<IOAuthService>()
                          .OAuthLogin();
        }

        public string Res_LoginButtonText => "Login";
    }
}