using System;
using System.Net;
using System.Runtime.Serialization;

namespace Avater
{
    [Serializable]
    internal class ApiException : Exception
    {
        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}