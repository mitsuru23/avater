using System;
using Avater.Models;
using Avater.Models.Interfaces;
using GitterSharp.Model;
using ReactiveUI;

namespace Avater
{
    [StoreFile("prefs.db")]
    public class CredentialStore : SettingsRepository, ICredentialStore
    {
        GitterUser _UserInfo;

        [Expires]

        public GitterUser UserInfo
        {
            get => _UserInfo;
            set => this.RaiseAndSetIfChanged(ref _UserInfo, value);
        }

        GitterToken _AccessToken;

        [Expires]
        public GitterToken AccessToken
        {
            get => _AccessToken;
            set => this.RaiseAndSetIfChanged(ref _AccessToken, value);
        }
    }
}