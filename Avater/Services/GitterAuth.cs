using System;
using System.Net;
using System.Threading;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Net.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using Avater.Models;
using Avater.Models.Interfaces;
using static Avater.ResourceStreamHelper;

namespace Avater
{
    public class GitterAuth : IOAuthService
    {
        private static HttpClient _webCli;
        private static WebServer _server;
        private static string _ackPage;
        private IObserver<GitterToken> _target;
        const string _gitterAuthPost = "https://gitter.im/login/oauth/token";
        string _appRedirectURL = "";
        const string _gitterOAuth = "https://gitter.im/login/oauth/authorize";

        public async Task OAuthLogin()
        {
            try
            {
                _appRedirectURL = await GetStringFromAvares("avares://Avater/avaterdev/redirect.url");
                _server.Run(DataReceived, _appRedirectURL + "/");
                var oauthKey = await GetStringFromAvares("avares://Avater/avaterdev/oauth.key");
                XPlatAPI.OpenInBrowser($"{_gitterOAuth}?client_id={oauthKey}&response_type=code&redirect_uri={_appRedirectURL}");
            }
            catch (Exception e)
            {
                _server.Stop();
                _target?.OnError(e);
            }
        }

        public GitterAuth()
        {
            Task.Run(() =>
            {
                _webCli = new HttpClient();
                _server = new WebServer();
            });
        }

        private string DataReceived(HttpListenerRequest arg)
        {
            if (_ackPage == null)
                _ackPage = GetStringFromAvares("avares://Avater/Assets/acknowledge.html").Result;

            Task.Run(async () =>
            {
                if (_target != null && arg.QueryString.HasKeys() & arg.QueryString.AllKeys.Contains("code"))
                {
                    try
                    {
                        var authCode = arg.QueryString["code"];
                        await DoCodeAuth(authCode);
                    }
                    catch (Exception e)
                    {
                        _target.OnError(e);
                    }
                }
            });

            return _ackPage;
        }


        private async Task DoCodeAuth(string authCode)
        {
            if (_target == null) return;

            try
            {
                var response = new GitterToken();

                var oauthKey = await GetStringFromAvares("avares://Avater/avaterdev/oauth.key");
                var oauthSecret = await GetStringFromAvares("avares://Avater/avaterdev/oauth.secret");

                var authDict = new Dictionary<string, string>()
                {
                    {"code", authCode},
                    {"client_id", oauthKey},
                    {"redirect_uri", _appRedirectURL},
                    {"client_secret", oauthSecret},
                    {"grant_type", "authorization_code"},
                };

                var formCont = new FormUrlEncodedContent(authDict);

                formCont.Headers.TryAddWithoutValidation("Accept", "application/json");

                var res = await _webCli.PostAsync(_gitterAuthPost, formCont);
                var access = await res.Content.ReadAsStringAsync();

                res.EnsureSuccessStatusCode();

                response = JsonConvert.DeserializeObject<GitterToken>(access, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                response.IsSuccessful = true;

                _target.OnNext(response);
                _server.Stop();

            }
            catch (Exception e)
            {
                _target.OnError(new ApiException($"Authentication Error", e));
                _server.Stop();
                return;
            }
        }

        public IDisposable Subscribe(IObserver<GitterToken> observer)
        {
            this._target = observer;
            return this;
        }

        public void Dispose()
        {
            _server.Stop();
            _webCli.Dispose();
        }
    }
}