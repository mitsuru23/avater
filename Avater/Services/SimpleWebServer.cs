/*
    Directly lifted from https://codehosting.net/blog/BlogEngine/post/Simple-C-Web-Server
    Licensed under The MIT License (MIT)
    Copyright (c) 2013 David's Blog (www.codehosting.net)
 */

using System;
using System.Net;
using System.Threading;
using System.Linq;
using System.Text;

namespace Avater
{
    public class WebServer
    {
        private HttpListener _listener;
        private Func<HttpListenerRequest, string> _responderMethod;

        public void Run(Func<HttpListenerRequest, string> method, params string[] prefixes)
            => Run(prefixes, method);

        public void Run(string[] prefixes, Func<HttpListenerRequest, string> method)
        {
            if (_listener != null) this.Stop();

            _listener = new HttpListener();

            // A responder method is required
            if (method == null)
                throw new ArgumentException("method");

            foreach (string s in prefixes)
                _listener.Prefixes.Add(s);

            _responderMethod = method;
            _listener.Start();

            ThreadPool.QueueUserWorkItem((o) =>
            {
                Console.WriteLine("Webserver running...");
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) =>
                        {
                            var ctx = c as HttpListenerContext;
                            try
                            {
                                string rstr = _responderMethod(ctx.Request);
                                byte[] buf = Encoding.UTF8.GetBytes(rstr);
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            catch
                            {
                            } // suppress any exceptions
                            finally
                            {
                                // always close the stream
                                ctx.Response.OutputStream.Close();
                                this.Stop();

                            }
                        }, _listener.GetContext());
                    }
                }
                catch { } // suppress any exceptions
            });
        }

        public void Stop()
        {
            try
            {
                _listener?.Stop();
                _listener?.Close();
            }
            finally
            {

            }
        }
    }
}