using System;
using System.Net;
using System.Threading;
using System.Linq;
using System.Text;
using Avater.Models.Interfaces;
using GitterSharp.Model;
using Avater.Models;

using ReactiveUI;
using System.Reflection;
using System.Collections.Generic;
using LiteDB;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Avater
{
    public abstract class SettingsRepository : ReactiveObject, IDisposable
    {
        string _connectionString, _colID;
        static Regex _nonAlpha = new Regex("[^a-zA-Z0-9_]", RegexOptions.Compiled);

        LiteRepository _db;

        List<string> _kvLookup;

        public SettingsRepository()
        {
            _kvLookup = new List<string>();

            var stor = this.GetType()
                   .GetCustomAttributes()
                   .Where(p => p.GetType() == typeof(StoreFileAttribute));

            if (stor.Any())
            {
                var storAt = (StoreFileAttribute)stor.First();

                _connectionString = storAt.TargetFile;
            }
            else
            {
                throw new InvalidProgramException($"{this.GetType()} must have a StoreFile attribute.");
            }

            _colID = _nonAlpha.Replace(this.GetType().FullName, "");

            _db = new LiteRepository($"Filename={_connectionString}; Password=" + _colID);

            var propslist = this.GetType().GetProperties().Where(p => p.DeclaringType == this.GetType());

            foreach (var props in propslist)
            {
                TimeSpan propExpTime = default;

                bool hasExpTime = false;

                var exp1 = props
                    .GetCustomAttributes()
                    .Where(p => p.GetType() == typeof(ExpiresAttribute));

                if (exp1.Any())
                {
                    var attrb = (ExpiresAttribute)exp1.First();
                    propExpTime = attrb.ExpiryTime;
                    hasExpTime = true;
                }

                var key = props.Name;

                _kvLookup.Add(key);

                var q1 = _db.FirstOrDefault<StorageKeyPair>(p => p.Key == key, _colID);
                if (q1 != null)
                {
                    this.GetType().GetProperty(q1.Key).SetValue(this, q1.Value);
                }
                else
                {
                    var kp_ = (new StorageKeyPair(key, null, hasExpTime, propExpTime));
                    _db.Insert(kp_, _colID);
                }
            }

            this.PropertyChanged += PropsChanged;
        }

        private void PropsChanged(object sender, PropertyChangedEventArgs e)
        {
            var res = _db.FirstOrDefault<StorageKeyPair>(p => p.Key == e.PropertyName, _colID);

            if (res != null)
            {
                res.Value = this.GetType().GetProperty(e.PropertyName).GetValue(this);
                _db.Update(res, _colID);
            }
        }



        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _db?.Dispose();
                }

                disposedValue = true;
            }
        }

        ~SettingsRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }

    internal class StorageKeyPair
    {

        [BsonId]
        public string Key { get; set; }
        public object Value { get; set; }
        public bool DoesExpire { get; set; }
        public TimeSpan Expiry { get; set; }

        public StorageKeyPair()
        {

        }

        public StorageKeyPair(string key, object value, bool doesExpire, TimeSpan expiry)
        {
            this.Key = key;
            this.Value = value;
            this.DoesExpire = doesExpire;
            this.Expiry = expiry;
        }
    }
}